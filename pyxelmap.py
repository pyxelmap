#!/usr/bin/env python
import sys, time, math, argparse, os, os.path
import PIL
from overviewer_core import world


def main():
    parser = argparse.ArgumentParser(description='Anvil 2d map generator.')
    parser.add_argument("--grid", type=int, metavar="BLOCKS", dest="gridStep",
                    help="Grid marks step")
    parser.add_argument("-f", "--filter", metavar="NAME", type=str, choices=chunkFilters.keys(), default="all",
                    help="Chunk filtering method")
    parser.add_argument("-F", "--blockfilter", metavar="NAME", type=str, choices=blockFilters.keys(),
                    help="Block filtering method")
    parser.add_argument("-g", "--arg", metavar="VALUE", type=int, action="append", default = [],
                    help="Chunk selection parameters")
    parser.add_argument("-i", "--in", dest="worldPath", metavar="PATH", type=str,
                    help="Source world directory")
    parser.add_argument("-o", "--out", dest="destPath", metavar="PATH", type=str, default=".",
                    help="Output directory")
    parser.add_argument("-r", "--region", metavar="NAME", type=str, default="overworld",
                    help="Region name")
    parser.add_argument("-v", "--verbose", action="store_true", default=False,
                    help="Print some progress info")
    args = parser.parse_args()
    
    try:
        srcWorld = world.World(args.worldPath)
        srcRegion = srcWorld.get_regionset(args.region)
    except Exception, ex:
        sys.exit("Error opening world")
    
    if not os.access(args.destPath, os.W_OK):
        sys.exit("Unable to write at %s" % args.destPath)
    
    if args.verbose:
        print "Preparing chunks"
    # filter out chunks
    
    if not args.blockfilter:
        blockFilter = None
        myChunks = [i for i in srcRegion.iterate_chunks() if chunkFilters[args.filter](i[0], i[1], i[2], args.arg)]
    else:
        blockFilter = blockFilters[args.blockfilter]
        myChunks = [i for i in srcRegion.iterate_chunks() if chunkBlockFilter(i[0], i[1], blockFilter, args.arg)]
    
    # find out northernmost, easternmost... chunks
    xChunks, yChunks, mtimes = zip(*myChunks)
    limitMinX, limitMinY, limitMaxX, limitMaxY = min(xChunks), min(yChunks), max(xChunks), max(yChunks)
    # resulting image dimensions
    dstSize = limitMinX * -16 + limitMaxX * 16 + 16, limitMinY * -16 + limitMaxY * 16 + 16
    dstXoffset, dstYoffset = 0, 0 # helper to move map around
    
    if args.verbose:
        print "Image size: %sx%s" % dstSize
    
    # canvas
    dstImg = PIL.Image.new("RGB", dstSize)
    dstDraw = PIL.ImageDraw.Draw(dstImg)

    axisStep = args.gridStep
    cnt = 0
    start = time.time()
    cleaner = ""
    for chx, chy, chmtime in myChunks:
        cnt += 1
        if args.verbose and not cnt % 10:
            # show some progress
            msg = "Chunk: \t%s,%s \t%s/%s \tETA %d/%ds " % \
                (chx, chy, cnt, len(myChunks), time.time() - start, (time.time() - start) * len(myChunks) / cnt)
            sys.stdout.write("\r"+cleaner+"\r"+msg)
            cleaner = " "*len(msg.expandtabs())
            sys.stdout.flush() # if you do not like this, better turn off whole of verbosity
            
        chunk = srcRegion.get_chunk(chx, chy)
        for y,x in chunkWalker():
            # world coords
            realX = (chx - limitMinX ) * 16 + x + dstXoffset
            realY = (chy - limitMinY ) * 16 + y + dstYoffset
            # image coords
            mapX = chx * 16 + x
            mapY = chy * 16 + y
            if blockFilter and not blockFilter(mapX, mapY, args.arg): continue
            
            #if chx, x, chy, y == 0,0,0,0: # mark world bellybutton # NOTE: slowdown
                #dstDraw.point((realX, realY), fill=(0, 0, 0))
                #continue
            
            # axes
            #if (not mapX % axisStep or not mapY % axisStep) and not x+y & 3: # dotted axis
            # TODO: draw in separate pass to save some time
            if axisStep and (((mapX+3) % axisStep < 7 and not mapY % axisStep) or ((mapY+3) % axisStep < 7 and not mapX % axisStep)): # cross axis
                dstDraw.point((realX, realY), fill=(255 - 30*abs((mapX+mapY+3)%axisStep-3) , 0, 0))
                continue
            
            # core job, find highest block and draw it
            bid, prevbid = 0, 0
            watermode = 0
            #print 
            for bid, prevbid, height in FallThru(chunk, x, y):
                if bid in blockId2color:
                    #print bid, prevbid
                    if bid in (8,9, 79):
                        if not watermode:
                            watermode = bid
                        continue
                    color, layer = getColor(bid, height)
                    if layer < 100:
                        #if realX == 0 or realY == 0: color = 0, 0, 0
                        if watermode:
                            color, garb = getColor(watermode, height)
                        dstDraw.point((realX, realY), fill=color)
                        # TODO: for debuging block ids
                        if color == (238, 130, 238):
                            print "\nunknown block id at %d/%d/%d : %s" % (realX, height, realY, bid)
                        break
    if args.verbose:
        sys.stdout.write("\r"+cleaner+"\rChunk: %d done in %ds\n"% (cnt, time.time() - start))
    
    del dstDraw
    dstImg.save(args.destPath, "PNG")


chunkFilters = {
    "all": lambda x,y,t,a: True,
    "square": lambda x,y,t,a: abs(x) < a[0] and abs(y) < a[0],
    "rect": lambda x,y,t,a: abs(x) < a[0] and abs(y) < a[1],
    "circle": lambda x,y,t,a: math.sqrt(x**2 + y**2) < a[0],
    "test": lambda x,y,t,a: x == 0 and y == 0,
    "single": lambda x,y,t,a: x == a[0] and y == a[1],
}

# special case, args will be block filter function
chunkBlockFilter = lambda x,y,f,a: \
                f(x*16,y*16, a) or \
                f(x*16+16,y*16, a) or \
                f(x*16,y*16+16, a) or \
                f(x*16+16,y*16+16, a) # anything within given block filter

blockFilters = {
    "all": None,
    "circle":  lambda x,y,a: math.sqrt(x**2 + y**2) < a[0],
}

planks = (160, 128, 0) # carpentry is kinda ubiquitous

# key - block id
# value tuple - color triple, layer, height tint flag
blockId2color = {
    1: ((128, 128, 128), 0, True), # stone
    2: ((51, 102, 0), 0, True), # grass
    3: ((102, 51, 0), 0, True), # dirt
    4: ((160, 160, 160), 10, True), # cobblestone
    5: (planks, 10, True), # planks
    7: ((0, 0, 0), 0, True), # bedrock
    8: ((0, 77, 153), 0, True), # water
    9: ((0, 77, 153), 0, True), # water
    10: ((255, 51, 51), 0, True), # lavavv
    11: ((255, 51, 51), 0, True), # lava
    12: ((160, 160, 80), 0, True), # sand
    13: ((153, 51, 153), 0, True), # gravel
    14: ((255, 255, 153), 2, False), # gold ore
    15: ((218, 188, 166), 2, False), # iron ore
    16: ((0, 0, 0), 2, False), # coal ore
    17: ((102, 51, 0), 3, False), # wood
    18: ((0, 153, 0), 3, False), # leaves
    21: ((0, 0, 153), 2, False), # lazuli ore
    22: ((0, 0, 204), 10, False), # lazuli block
    24: ((204, 204, 0), 10, True), # sandstone
    27: ((200, 200, 212), 10, True), # power rail
    28: ((200, 200, 212), 10, True), # switch rail
    35: ((224, 224, 224), 10, True), # wool
    42: ((200, 200, 212), 10, False), # iron block
    45: ((153, 0, 0), 10, True), # red bricks
    60: ((255, 153, 51), 10, True), # irigated dirt
    66: ((200, 200, 212), 10, True), # rails
    73: ((153, 0, 0), 0, False), # redstone ore
    74: ((153, 0, 0), 0, False), # redstone ore
    78: ((204, 204, 204), 0, True), # snow
    79: ((153, 204, 255), 0, True), # ice
    80: ((204, 204, 204), 0, True), # snow
    91: ((204, 102, 0), 3, False), # pumpkin
    103: ((128, 255, 0), 3, False), # melon
    126: (planks, 10, True), # wood slab
    128: ((204, 204, 0), 10, True), # sandstone stairs
    134: (planks, 10, True), # wood
    135: (planks, 10, True), # wood
    136: (planks, 10, True), # wood
}
unknown = (238, 130, 238), 20, False


def getColor(bid, height):
    """Decide pixel color for block of given type in given height"""
    color, layer, tint = blockId2color.get(bid, unknown)
    #color = blockId2color.get(bid, unknown)
    if tint:
        heightPart = (height) / 64.0
        #return tuple([int(i * heightPart) for i in color]), layer
        return (int(color[0] * heightPart), int(color[1] * heightPart), int(color[2] * heightPart)), layer
    else:
        return color, layer


def FallThru(chunk, x, y):
    """Iterate blocks top to bottom"""
    height = len(chunk["Sections"]) * 16
    prev = 0
    for sect in reversed(chunk["Sections"]):
        for layer in reversed(sect["Blocks"]):
            cur = layer[y,x]
            yield cur, prev, height
            prev = cur
            height -= 1


def chunkWalker():
    """Iterate 16x16 matrix"""
    for i in range(16):
        for j in range(16):
            yield j,i


if __name__ == "__main__":
    main()
